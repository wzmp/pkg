# How to install

## Setup Registry

### NPM
```bash
echo @wzmp:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm i @wzmp/zmp
```

### Yarn
```bash
echo \"@wzmp:registry\" \"https://gitlab.com/api/v4/packages/npm/\" >> .yarnrc
yarn add @wzmp/zmp
```
